/*---------------------------------------------------------
 * OpenERP web_graph
 *---------------------------------------------------------*/

openerp.web_graph = function (instance) {
'use strict';

var _lt = instance.web._lt;
var _t = instance.web._t;

instance.web.views.add('graph', 'instance.web_graph.GraphView');
instance.web_graph.GraphView = instance.web.View.extend({
    template: 'GraphView',
    display_name: _lt('Graph'),
    view_type: 'graph',

    events: {
        'click .graph_measure_selection li' : function (event) {
            var new_measure = event.target.attributes['data-measure'].nodeValue;
            this.measure = (new_measure == 'quantity') ? null : new_measure;
            this.render_view();
        },
        'click .graph_mode_selection li' : function (event) {
            this.mode = event.target.attributes['data-mode'].nodeValue;
            this.render_view();
        },
        'click .graph_rows_selection li' : function (event) {
            var row = event.target.attributes['data-row'].nodeValue;
            if (row == '_clear_rows') {
                this.x_groups = [];
            } else {
                this.x_groups.push(row);
            }
            this.get_data().then(this.render_view.bind(this));
        },
        'click .graph_columns_selection li' : function (event) {
            var col = event.target.attributes['data-column'].nodeValue;
            if (col == '_clear_columns') {
                this.y_groups = [];
            } else {
                this.y_groups.push(col);
            }
            this.get_data().then(this.render_view.bind(this));
        },
    },

    init: function (parent, dataset, view_id, options) {
        this._super(parent);
        this.set_default_options(options);
        this.dataset = dataset;
        this.view_id = view_id;
        this.domain = [];
        this.context = {};

        this.mode = 'bar_chart';  // data, bar_chart, line_chart, pie_chart
        this.x_groups = [];
        this.y_groups = [];
        this.default_x_groups = [];

        this.measures_list = [];
        this.measure = null;  // null -> count entries
        this.data = [];

        this.fields = [];
        this.all_fields = null;
    },

    view_loading: function (fields_view_get) {
        var self = this;

        this.model = new instance.web.Model(fields_view_get.model, {group_by_no_leaf: true});

        _.each(fields_view_get.arch.children, function (field) {
            if ('name' in field.attrs) {
                if ('operator' in field.attrs) {
                    self.measures_list.push(field.attrs.name);
                    self.measure = field.attrs.name;
                } else {
                    self.default_x_groups.push(field.attrs.name);
                }
            }
        });

        self.x_groups = self.default_x_groups;

        this.fields_view = fields_view_get;
        this.$el.addClass(this.fields_view.arch.attrs['class']);

        var width = 800; //this.$el.parent().width();
        this.$el.css('width', width);
        this.container = this.$('#editor-render-body');
        this.container.css({
            width: width,
            height: Math.min(500, width * 0.8)
        });

        var load_view = instance.web.fields_view_get({
            model: this.model,
            view_type: 'search',
        });


        $.when(load_view).then(function (search_view) {
            var groups = _.select(search_view.arch.children, function (c) {
                return (c.tag == 'group') && (c.attrs.string != 'Display');
            });

            var row_selection = self.$('.graph_rows_selection');
            var col_selection = self.$('.graph_columns_selection');
            _.each(groups, function(g) {
                _.each(g.children, function (g) {
                    if (g.attrs.context) {
                        var field_id = py.eval(g.attrs.context).group_by;

                        var field_name = g.attrs.string;
                        var field_row = '<li><a data-row="' +
                            field_id + '" href="#">' + field_name + '</a></li>';
                        var field_col = '<li><a data-column="' +
                            field_id + '" href="#">' + field_name + '</a></li>';
                        row_selection.prepend(field_row);
                        col_selection.prepend(field_col);
                    }
                });
            });
        });

        return this.model.call('fields_get', []).then(function (fields) {
            self.all_fields = fields;

            var measure_selection = self.$('.graph_measure_selection');
            _.each(self.measures_list, function (measure) {
                var new_measure =  '<li><a data-measure="' +
                    measure + '" href="#">' + fields[measure].string + '</a></li>';

                measure_selection.append(new_measure);
            });

        }).then(self.get_data.bind(self));
    },

    render_view: function () {
        var dispatch = {
                'data': this.render_data,
                'bar_chart': this.render_bar_chart,
                'line_chart': this.render_line_chart,
                'pie_chart': this.render_pie_chart
            };

        console.log('X-groups:',this.x_groups, 'Y-groups:', this.y_groups,
                    'measure:', this.measure, 'data:', this.data);

        this.container.empty();
        dispatch[this.mode].bind(this)();
    },

    get_data: function () {
        var domain = new  instance.web.CompoundDomain(this.domain),
            self = this,
            view_fields = self.x_groups.concat(self.y_groups, self.measures_list);

        return query_groups(this.model, view_fields, domain, this.x_groups.concat(this.y_groups))
            .then(function (data) {
                self.data = data;
            });
    },

    // Display the data in tabular form, using the groupbys in x and y.
    //    it assumes that the data is completely loaded in the var this.data 
    render_data: function () {
        var self = this,
            dim_x = this.x_groups.length,
            dim_y = this.y_groups.length;

        // Helper functions, to handle cells, lines and blocks.  
        //     A cell is a string of the type '<td + styling info>content</td>'
        //     A line is a list of cells
        //     A block is a list of lines
        function make_cell (content, is_border, col_span, row_span) {
            var cell = '<td';
            if (is_border) {
                cell += ' class="graph_border"';
            }
            if (row_span !== undefined) {
                cell += ' rowspan="' + row_span + '"';
            }
            if (col_span !== undefined) {
                cell += ' colspan="' + col_span + '"';
            }
            return cell + '>' + content + '</td>';
        }

        // puts two blocks side by side 
        function concat_blocks(block1, block2) {
            var min_height = Math.min(block1.length, block2.length);
            var tallest = block1.length > block2.length ? block1 : block2;
            
            var partial_result = _.map(_.range(min_height), function (index) {
                    return block1[index].concat(block2[index]);
                });

            return partial_result.concat(tallest.slice(min_height));
        }

        function block_to_html(block) {
            return _.map(block, function (cells) {
                return '<tr>' + cells.join('') + '</tr>';
            }).join('');
        }

        function get_desc(field) {
            return self.all_fields[field].string;
        }

        // builds top left block 
        var top_left;

        var y_groupbys = _.map(this.y_groups, function (groupby) {
            return [make_cell(get_desc(groupby), true)];
        });
        var x_groupbys = [_.map(this.x_groups, function (groupby) {
            return make_cell(get_desc(groupby), true);
        })];

        if ((dim_x === 0) && (dim_y === 0)) {
            top_left = [[make_cell('', true)]];
        } else if (dim_x === 0) {
            top_left = y_groupbys;
        } else if (dim_y === 0) {
            top_left = x_groupbys;
        } else {
            var empty_cell = [[make_cell('', true, dim_x, dim_y)]];
            top_left = concat_blocks(empty_cell, y_groupbys)
                   .concat(concat_blocks(x_groupbys, [make_cell('', true)]));
        }

        // Builds top right and bottom left blocks.
        //    Doing so requires two steps:
        //    1. it scans the data to extract the row and column headers
        //    2. then it builds the actual blocks
        //
        //   The headers are encoded in objects with 3 attributes:
        //   1. field: type of the header, from the model (i.e. section_id)
        //   2. value: its value (i.e. "New")
        //   3. children : a list of sub headers

        var col_infos = [];  // will be used to build top right block

        function scan_data (raw_data) {
            var grouped_on = raw_data[0].attributes.grouped_on;

            if (typeof grouped_on === 'undefined') {
                return [{value: 'Total', children: []}];
            }
            if ($.inArray(grouped_on, self.x_groups) == -1) {
                col_infos = col_infos.concat(scan_col_headers(raw_data));
                return [];
            }
            return _.map(raw_data, function (data) {
                var new_header = {
                    field: data.attributes.grouped_on,
                    value: data.attributes.value[1],
                    children : [],
                };
                if (data.attributes.has_children) {
                    new_header.children = scan_data(data.subgroups_data);
                }
                return new_header;
            });
        }
        
        function build_bottom_left(tree) {
            if (tree.length === 0) {
                return [[make_cell('Total', true)]];
            }
            return _.flatten(_.map(tree, function (header) {
               if (header.children.length === 0) {
                    var span = (dim_y === 0) ? 1 : 2;
                    return [[make_cell(header.value, true, span)]];
               } else {
                    var header_cell = make_cell(header.value, true,
                                                1, header.children.length);
                    var sub_headers = build_bottom_left(header.children);
                    return concat_blocks([[header_cell]], sub_headers);
               }
            }), true);
        }

        var bottom_left = build_bottom_left(scan_data(this.data));

        function scan_col_headers (raw_data) {
            return _.map(raw_data, function (data) {
                var new_header = {
                    field: data.attributes.grouped_on,
                    value: data.attributes.value[1],
                    children : [],
                };
                if (data.attributes.has_children) {
                    new_header.children = scan_col_headers(data.subgroups_data);
                }
                return new_header;
            });
        }

        function merge_trees (headers) {
            var grouped_trees = _.groupBy(headers,'value');

            return _.map(grouped_trees, function (trees) {
                if (trees.length == 1) {
                    return trees[0];
                }
                var new_tree = {
                    field: trees[0].field,
                    value: trees[0].value,
                    children : []
                };
                if (trees[0].children.length > 0) {
                    new_tree.children = merge_trees(_.flatten(_.map(trees, function (t) {
                        return t.children;
                    }), true));
                }
                return new_tree;
            });
        }

        function build_top_right(tree) {
            var blocks = _.map(tree, function (subtree) {
                if (subtree.children.length === 0) {
                    return [[make_cell(subtree.value, true)]];
                } else {
                    var cell = make_cell(subtree.value, true, subtree.children.length);
                    var sub_headers = build_top_right(subtree.children);
                    return [[cell]].concat(sub_headers);
                }
            });
            return _.reduce(blocks, concat_blocks);
        }

        var top_right;
        if (col_infos.length === 0) {
            var label = (self.measure) ? get_desc(self.measure) : 'Cantidad';
            top_right = [[make_cell(label, true)]];
        } else {
            top_right = build_top_right(merge_trees(col_infos));
        }

        // build main block, with the real data
        // var main_block = [[make_cell('data', false)]];

        // put everything together
        var htmltable = '<table>' +
                      block_to_html(concat_blocks(top_left, top_right)) +
                      // block_to_html(concat_blocks(bottom_left, main_block))
                      block_to_html(bottom_left) +
                      '</table>';

        this.container.append(htmltable);
    },

    format_data:  function (datapt) {
        var val = datapt.attributes;
        if($.isArray(datapt.attributes.value)) {
        	return {
                x: datapt.attributes.value[1],
                y: (this.measure) ? val.aggregates[this.measure] : val.length,
            };
        } else {
        	return {
                x: datapt.attributes.value,
                y: (this.measure) ? val.aggregates[this.measure] : val.length,
            };
        }
    },

    render_bar_chart: function () {
        var formatted_data = [{
                key: 'Bar chart',
                values: _.map(this.data, this.format_data.bind(this)),
            }];

        this.container.append('<svg id="chart_graph"></svg>');

        nv.addGraph(function () {
            var chart = nv.models.discreteBarChart()
                .tooltips(false)
                .showValues(true)
                .staggerLabels(true)

            d3.select('#chart_graph')
                .datum(formatted_data)
                .call(chart);

            nv.utils.windowResize(chart.update);
            return chart;
        });
    },

    render_pie_chart: function () {
        var formatted_data = _.map(this.data, this.format_data.bind(this));

        this.container.append('<svg id="chart_graph"></svg>');

        nv.addGraph(function () {
            var width = 550,
                height = 550;

            var chart = nv.models.pieChart()
                .color(d3.scale.category10().range())
                .width(width)
                .height(height);

            d3.select('#chart_graph')
                .datum(formatted_data)
                .transition().duration(1200)
                .attr('width', width)
                .attr('height', height)
                .call(chart);

            nv.utils.windowResize(chart.update);
            return chart;
        });
    },

    render_line_chart: function () {
        var measure_label =
            (this.measure) ? this.all_fields[this.measure].string : 'Cantidad';

        var formatted_data = [{
                key: measure_label,
                values: _.map(this.data, this.format_data.bind(this))
            }];

        this.container.append('<svg id="chart_graph"></svg>');

        nv.addGraph(function () {
            var width = 600,
                height = 300;

            var chart = nv.models.lineChart()
                .x(function (d,u) { return u; })
                .width(width)
                .height(height)
                .margin({top: 30, right: 20, bottom: 20, left: 60});

            d3.select('#chart_graph')
                .attr('width', width)
                .attr('height', height)
                .datum(formatted_data)
                .call(chart);

            return chart;
          });
    },

    // render the graph using the domain, context and group_by
    // calls the 'graph_data_get' python controller to process all data
    // TODO: check is group_by should better be in the context
    do_search: function (domain, context, group_by) {
        this.domain = domain;
        this.context = context;

        if (group_by.length >= 1) {
            this.x_groups = group_by;
        } else {
            this.x_groups = this.default_x_groups;
        }

        this.get_data().then(this.render_view.bind(this));
    },

    do_show: function () {
        this.do_push_state({});
        return this._super();
    },
});


function is_not_empty (group) {
    return group.attributes.length > 0;
}

function query_groups (model, fields, domain, groupings) {
    return model.query(fields)
        .filter(domain)
        .group_by(groupings)
        .then(function (results) {
            var non_empty_results = _.filter(results, is_not_empty);
            if (groupings.length <= 1) {
                return non_empty_results;
            } else {
                var get_subgroups = $.when.apply(null, _.map(non_empty_results, function (result) {
                    var new_domain = result.model._domain;
                    var new_groupings = groupings.slice(1);
                    return query_groups(model, fields,new_domain, new_groupings).then(function (subgroups) {
                        result.subgroups_data = subgroups;
                    });
                }));
                return get_subgroups.then(function () {
                    return non_empty_results;
                });
            }
        });
}

};
