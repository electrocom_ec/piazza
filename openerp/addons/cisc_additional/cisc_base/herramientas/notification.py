
# -*- encoding: utf-8 -*-
########################################################################
#                                                                       
# @authors: Christopher Ormaza                                                                           
# Copyright (C) 2012  Ecuadorenlinea.net                                 
#                                                                       
#This program is free software: you can redistribute it and/or modify   
#it under the terms of the GNU General Public License as published by   
#the Free Software Foundation, either version 3 of the License, or      
#(at your option) any later version.                                    
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#                                                                       
#This program is distributed in the hope that it will be useful,        
#but WITHOUT ANY WARRANTY; without even the implied warranty of         
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
#GNU General Public License for more details.                           
#                                                                       
#You should have received a copy of the GNU General Public License      
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv
from osv import fields
import decimal_precision as dp
from tools.translate import _

class util_notification(osv.osv):
    _name = 'util.notification'

    _description = 'util.notification'

    _columns = {
            'name':fields.char('Asunto', size=255, required=True, readonly=True),
            'header': fields.text('Cabecera'),
            'footer': fields.text('Pie de Página'),
            'user_mails_ids':fields.many2many('res.users', 'notificationuba_users_rel', 'report_id', 'user_id', 'Usuarios/Envío de Correos'),
            'groups_mails_ids':fields.many2many('res.groups', 'notificationuba_group_rel', 'report_id', 'group_id', 'Grupos/Enví­o de Correos'),
        }
util_notification()