# -*- encoding: utf-8 -*-

from openerp.osv import fields, osv
import re
import base64
from tools.translate import _
from datetime import datetime, timedelta
from ast import literal_eval
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT

def check_password(login, password):
    if len(password) >= 8:
        if re.match(".*[a-z].*", password) and re.match(".*[A-Z].*", password) and re.match(".*[0-9].*", password):
            return True#self.pool.get('res.users').write(cr, uid, user.user_id.id, {'password': user.new_passwd})
        else:
            raise osv.except_osv("Error",u"La contraseña debe contener: \nlongitud minima de 8 caracteres, \n1 numero[0-9], \n1 letra en mayúscula[A-Z]")
    else:
        raise osv.except_osv("Error",u"La contraseña debe contener: \nlongitud minima de 8 caracteres, \n1 numero[0-9], \n1 letra en mayúscula[A-Z]")
    
            
def random_token():
    # the token has an entropy of about 120 bits (6 bits/char * 20 chars)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    return ''.join(random.choice(chars) for i in xrange(20))

def now(**kwargs):
    dt = datetime.now() + timedelta(**kwargs)
    return dt.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

class change_password_wizard(osv.TransientModel):
    """
        A wizard to manage the change of users' passwords
    """
    _inherit = "change.password.wizard"
    
    def default_get(self, cr, uid, fields, context=None):
        if context == None:
            context = {}
        user_ids = context.get('active_ids', [])
        wiz_id = context.get('active_id', None)
        res = []
        users = self.pool.get('res.users').browse(cr, uid, user_ids, context=context)
        for user in users:
            res.append((0, 0, {
                'wizard_id': wiz_id,
                'user_id': user.id,
                'user_login': user.login,
            }))
        return {'user_ids': res}


    def change_password_button(self, cr, uid, id, context=None):
        wizard = self.browse(cr, uid, id, context=context)[0]
        user_ids = []
        for user in wizard.user_ids:
            user_ids.append(user.id)
        self.pool.get('change.password.user').change_password_button(cr, uid, user_ids, context=context)
        return {
            'type': 'ir.actions.act_window_close',
        }

class change_password_user(osv.TransientModel):
    """
        A model to configure users in the change password wizard
    """

    _inherit = 'change.password.user'
    
    _defaults = {
        'new_passwd': '',
    }

    def change_password_button(self, cr, uid, ids, context=None):
        user_ids = context.get('active_ids', [])
        userl = self.pool.get('res.users').browse(cr, uid, user_ids, context=context)[0]
        for user in self.browse(cr, uid, ids, context=context):
            password = user.new_passwd
            if check_password(userl.login,password):
                self.pool.get('res.users').write(cr, uid, user.user_id.id, {'password': password})
            

class res_users(osv.Model):
    _inherit = 'res.users'

    #def _get_state(self, cr, uid, ids, name, arg, context=None):
    #    res = {}
    #    for user in self.browse(cr, uid, ids, context):
    #        res[user.id] = ('active' if user.login_date else 'new')
    #   return res

    #_columns = {
    #    'state': fields.function(_get_state, string='Status', type='selection',
    #                selection=[('new', 'Never Connected'), ('active', 'Activated')]),
    #}

    
    
    def signup(self, cr, uid, values, token=None, context=None):
        """ signup a user, to either:
            - create a new user (no token), or
            - create a user for a partner (with token, but no user for partner), or
            - change the password of a user (with token, and existing user).
            :param values: a dictionary with field values that are written on user
            :param token: signup token (optional)
            :return: (dbname, login, password) for the signed up user
        """
        if token:
            password = values.get('password')
            #if check_password(values.get('login'), password, False):
                
            # signup with a token: find the corresponding partner id
            res_partner = self.pool.get('res.partner')
            partner = res_partner._signup_retrieve_partner(
                            cr, uid, token, check_validity=True, raise_exception=True, context=None)
            # invalidate signup token
            partner.write({'signup_token': False, 'signup_type': False, 'signup_expiration': False})

            partner_user = partner.user_ids and partner.user_ids[0] or False
            if partner_user:
                # user exists, modify it according to values
                values.pop('login', None)
                values.pop('name', None)
                partner_user.write(values)
                return (cr.dbname, partner_user.login, values.get('password'))
            else:
                # user does not exist: sign up invited user
                values.update({
                    'name': partner.name,
                    'partner_id': partner.id,
                    'email': values.get('email') or values.get('login'),
                })
                if partner.company_id:
                    values['company_id'] = partner.company_id.id
                    values['company_ids'] = [(6,0,[partner.company_id.id])]
                self._signup_create_user(cr, uid, values, context=context)
        else:
            # no token, sign up an external user
            values['email'] = values.get('email') or values.get('login')
            self._signup_create_user(cr, uid, values, context=context)
    
        return (cr.dbname, values.get('login'), values.get('password'))
        

    def _signup_create_user(self, cr, uid, values, context=None):
        """ create a new user from the template user """
        ir_config_parameter = self.pool.get('ir.config_parameter')
        template_user_id = literal_eval(ir_config_parameter.get_param(cr, uid, 'auth_signup.template_user_id', 'False'))
        assert template_user_id and self.exists(cr, uid, template_user_id, context=context), 'Signup: invalid template user'

        # check that uninvited users may sign up
        if 'partner_id' not in values:
            if not literal_eval(ir_config_parameter.get_param(cr, uid, 'auth_signup.allow_uninvited', 'False')):
                raise SignupError('Signup is not allowed for uninvited users')

        assert values.get('login'), "Signup: no login given for new user"
        assert values.get('partner_id') or values.get('name'), "Signup: no name or partner given for new user"

        # create a copy of the template user (attached to a specific partner_id if given)
        values['active'] = True
        return self.copy(cr, uid, template_user_id, values, context=context)

    #def reset_password(self, cr, uid, login, context=None):
    #    """ retrieve the user corresponding to login (login or email),
    #        and reset their password
    #   """
    #    user_ids = self.search(cr, uid, [('login', '=', login)], context=context)
    #    if not user_ids:
    #        user_ids = self.search(cr, uid, [('email', '=', login)], context=context)
    #    if len(user_ids) != 1:
    #        raise Exception('Reset password: invalid username or email')
    #    return self.action_reset_password(cr, uid, user_ids, context=context)

    def action_reset_password(self, cr, uid, ids, context=None):
        """ create signup token for each user, and send their signup url by email """
        # prepare reset password signup
        res_partner = self.pool.get('res.partner')
        partner_ids = [user.partner_id.id for user in self.browse(cr, uid, ids, context)]
        res_partner.signup_prepare(cr, uid, partner_ids, signup_type="reset", expiration=now(days=+1), context=context)

        if not context:
            context = {}

        #send email to users with their signup url
        template = False
        if context.get('create_user'):
            try:
               template = self.pool.get('ir.model.data').get_object(cr, uid, 'electronic_invoicing_sri', 'set_password_email_ext')
            except ValueError:
                pass
        if not bool(template):
            template = self.pool.get('ir.model.data').get_object(cr, uid, 'auth_signup', 'reset_password_email')
        mail_obj = self.pool.get('mail.mail')
        assert template._name == 'email.template'
        for user in self.browse(cr, uid, ids, context):
            if not user.email:
                raise osv.except_osv(_("Cannot send email: user has no email address."), user.name)
            mail_id = self.pool.get('email.template').send_mail(cr, uid, template.id, user.id, True, context=context)
            mail_state = mail_obj.read(cr, uid, mail_id, ['state'], context=context)
            if mail_state and mail_state['state'] == 'exception':
                raise osv.except_osv(_("Cannot send email: no outgoing email server configured.\nYou can configure it under Settings/General Settings."), user.name)
            else:
                return True

    #def create(self, cr, uid, values, context=None):
    #    # overridden to automatically invite user to sign up
    #    user_id = super(res_users, self).create(cr, uid, values, context=context)
    #    user = self.browse(cr, uid, user_id, context=context)
    #    if context and context.get('reset_password') and user.email:
    #        ctx = dict(context, create_user=True)
    #        self.action_reset_password(cr, uid, [user.id], context=ctx)
    #    return user_id
    