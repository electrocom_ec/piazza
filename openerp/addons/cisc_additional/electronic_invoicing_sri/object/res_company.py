# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2014  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import osv
from osv import fields
import decimal_precision as dp
from tools.translate import _
import time

class res_company(osv.osv):
    '''
    Open ERP Model
    '''
    _inherit = 'res.company'
    _description = 'res.company'

    _columns = {
            'path_digital_certificate':fields.char('Certificado digital', size=255, required=True),
            'password_certificate':fields.char('Contraseña', size=255, required=True), 
            'mode':fields.selection([
                ('generation_file','Generación archivo Texto'),
                ('interaction_db','Acceso DB'),
                 ],    'Modalidad', select=True, required=True), 
            #'path_invoice':fields.char('Facturas', size=255),
            #'path_refund':fields.char('Nota crédito', size=255),
            #'path_debit':fields.char('Nota débito', size=255),
            #'path_guide':fields.char('Guia de remisión', size=255),
            #'path_retention':fields.char('Retenciones', size=255),
            'user':fields.char('Usuario', size=255),
            'password':fields.char('Contraseña', size=255),
            'port':fields.char('Puerto', size=255),
            'host':fields.char('Servidor', size=255),
            'database':fields.char('Base de Datos', size=255),
            'path_temp':fields.char('Comprobantes para firmar electrónicamente', size=255, required=True),
            'reason_social':fields.char('Razón social', size=255, required=True),
            'email_notification':fields.char('Email notificación', size=150), 
        }
    
    _defaults={
            'mode':'generation_file'
               }
    
res_company()