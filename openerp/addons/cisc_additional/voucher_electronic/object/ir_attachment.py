# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2014  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import osv
from osv import fields
from tools.translate import _
from openerp.tools.misc import ustr
import re
from document.content_index import cntIndex


class ir_attachment(osv.osv):
    
    _inherit = 'ir.attachment'

    def read_contingency_file(self, cr, uid, valor, context=None):
        electronic_obj = self.pool.get('electronic.invoicing.sri')
        valores = valor.split("\n")
        electronic_obj.read_file_upload_clave_contingente(cr, uid, valores, context=None)
    
    def leerArchivo(self, cr, uid, valor, context=None):
        electronic_obj = self.pool.get('electronic.invoicing.sri')
        valores = valor.split("\n")
        if re.match(".*factura.*",valores[0].lower()):
            electronic_obj.read_file_upload_invoice(cr, uid, valores[1:], context=None)
        elif re.match(".*retencion.*",valores[0].lower()):
            electronic_obj.read_file_upload_retention(cr, uid, valores[1:], context=None)
        elif re.match(".*notacredito.*", valores[0].lower()):
            electronic_obj.read_file_upload_credit_note(cr, uid, valores[1:], context=None)
        elif re.match(".*notadebito.*", valores[0].lower()):
            electronic_obj.read_file_upload_debit_note(cr, uid, valores[1:], context=None)
        elif re.match(".*guiaremision.*", valores[0].lower()):
            electronic_obj.read_file_upload_remission_guide(cr, uid, valores[1:], context=None)
        else:
            raise osv.except_osv(_('Error'),_("Favor Revisar el Archivo de texto"))

    def _data_get(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        result = {}
        location = self.pool.get('ir.config_parameter').get_param(cr, uid, 'ir_attachment.location')
        bin_size = context.get('bin_size')
        for attach in self.browse(cr, uid, ids, context=context):
            if location and attach.store_fname:
                result[attach.id] = self._file_read(cr, uid, location, attach.store_fname, bin_size)
            else:
                result[attach.id] = attach.db_datas
        return result
    
    def _index(self, cr, uid, data, datas_fname, file_type):
        mime, icont = cntIndex.doIndex(data, datas_fname,  file_type or None, None)
        icont_u = ustr(icont)
        self.content = icont_u
        return mime, icont_u
    
    def _data_set(self, cr, uid, id, name, value, arg, context=None):
        # We dont handle setting data to null
        if not value:
            return True
        if context is None:
            context = {}
        location = self.pool.get('ir.config_parameter').get_param(cr, uid, 'ir_attachment.location')
        file_size = len(value.decode('base64'))
        if location:
            attach = self.browse(cr, uid, id, context=context)
            if attach.store_fname:
                self._file_delete(cr, uid, location, attach.store_fname)
            fname = self._file_write(cr, uid, location, value)
            super(ir_attachment, self).write(cr, uid, [id], {'store_fname': fname, 'file_size': file_size}, context=context)
        else:
            super(ir_attachment, self).write(cr, uid, [id], {'db_datas': value, 'file_size': file_size}, context=context)
        if context.get('is_document'):
             self.leerArchivo(cr ,uid, self.content)
        elif context.get('is_clave_contingente'):
             self.read_contingency_file(cr ,uid, self.content)
            
        return True
    
    def default_get(self, cr, uid, fields_list, context=None):
        if not context:
            context={}
        values = super(ir_attachment, self).default_get(cr, uid, fields_list, context)
        if context.get('is_document'):
            values['is_document']=True
        elif context.get('is_clave_contingente'):
            values['is_clave_contingente']=True
        return values
    
    def _name_get_resname(self, cr, uid, ids, object, method, context):
        data = {}
        for attachment in self.browse(cr, uid, ids, context=context):
            
            if not attachment.res_model and not attachment.res_id:
                model_object = 'res.company'
                res_id = 1
                self.write(cr,uid,[attachment.id],{'res_model':model_object, 'res_id':None } ,context = None )
            else:    
                model_object = attachment.res_model
                res_id = attachment.res_id
            if model_object and res_id:
                model_pool = self.pool.get(model_object)
                res = model_pool.name_get(cr,uid,[res_id],context)
                res_name = res and res[0][1] or False
                if res_name:
                    field = self._columns.get('res_name',False)
                    if field and len(res_name) > field.size:
                        res_name = res_name[:field.size-3] + '...' 
                data[attachment.id] = res_name
            else:
                data[attachment.id] = False
            
        return data

    _columns={              
        'is_document':fields.boolean('Documento', required=False),
        'datas': fields.function(_data_get, fnct_inv=_data_set, string='Documento', type="binary", nodrop=True),
        'res_name': fields.function(_name_get_resname, type='char', size=128, string='Resource Name', store=True),
        'is_clave_contingente': fields.boolean('Clave contingente', required=False),
        'number_doc': fields.char('Numero de Documento', size=64, readonly=True),
        'type_doc': fields.char('Documento', size=64, readonly=True),
        'view': fields.char('Vista', size=64, required=False, readonly=False),
    }
    
ir_attachment()
